// Estructura de los datos compartidos
//
//////////////////////////////////////////////////////////////////////

#pragma once
#include "Esfera.h"
#include "Raqueta.h"

class DatosMemCompartida
{
public:
	Esfera esfera1,esfera2;
	Raqueta raqueta1,raqueta2;
	int accion1,accion2; //1 arriba, 0 nada, -1 abajo
};
