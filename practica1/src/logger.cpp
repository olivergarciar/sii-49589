//Programa logger

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

typedef struct mensaje {
	int jugador;
	int puntos;
} mensaje;


int main(int argc, char **argv) {
	int fd;
	
	// crea el FIFO
	if (mkfifo("/tmp/FIFO", 0777)<0) {
		perror("No puede crearse el FIFO");
		return(1);
	}
	/* Abre el FIFO */
	if ((fd=open("/tmp/FIFO", O_RDONLY))<0) {
		perror("No puede abrirse el FIFO");
		return(1);
	}

	mensaje m;
	while (read(fd, &m, sizeof(m))==sizeof(m)) {
		printf("Jugador %d marca 1 punto, lleva un total de %d puntos \n", m.jugador, m.puntos); 


	}

	if((close(fd))<0)
	{
		perror("Error al cerrar el fifo desde logger");
		exit(EXIT_FAILURE);
	}
	if((unlink("/tmp/FIFO"))<0)
	{
		perror("Error en el unlink del fifo en logger");
		exit(EXIT_FAILURE);
	}
}





	
