//Oliver Garcia Rodriguez 2018
//
// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/wait.h>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
/////////////////////////////////////////////////////////////////////

int fd;


typedef struct mensaje {
	int jugador;
	int puntos;
} mensaje;

mensaje m;


void *comandos(void * arg)
{
	CMundoServidor *p=(CMundoServidor*)arg;
	p->RecibeComandos();
}

CMundoServidor::CMundoServidor()
{
	Init();
}

CMundoServidor::~CMundoServidor()
{	
	if(-1 == close(fd))
	{
		perror("Error al cerrar el fifo en MundoServidor");
		exit(EXIT_FAILURE);
	}

	if(-1 == close(fd1))
	{
		perror("Error al cerrar el fifo1 en MundoServidor");
		exit(EXIT_FAILURE);
	}

	if(-1 == close(fd2))
	{
		perror("Error al cerrar el fifo2 en MundoServidor");
		exit(EXIT_FAILURE);
	}
}

void CMundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
//	esfera.Dibuja();
	for(int i=0;i<numEsferas;i++)
	{
		esfera[i].Dibuja();
	}

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoServidor::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	for(int i=0;i<numEsferas;i++)
	{
		esfera[i].Mueve(0.025f);
	}

	for(int i=0;i<paredes.size();i++)
	{
		for(int u=0;u<numEsferas;u++)
		{
			paredes[i].Rebota(esfera[u]);
		}
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	for(int i=0;i<numEsferas;i++)
	{
		jugador1.Rebota(esfera[i]);
	}
	for(int i=0;i<numEsferas;i++)
	{
		jugador2.Rebota(esfera[i]);
	}
	for(int i=0;i<numEsferas;i++)
	{
		if(fondo_izq.Rebota(esfera[i]))
		{
			esfera[i].centro.x=0;
			esfera[i].centro.y=rand()/(float)RAND_MAX;
			esfera[i].velocidad.x=2+2*rand()/(float)RAND_MAX;
			esfera[i].velocidad.y=2+2*rand()/(float)RAND_MAX;
			puntos2++;
			//Escribe en el FIFO
			m.jugador=2;
			m.puntos=puntos2;
			if(-1 == write(fd, &m, sizeof(m)))
			{
				perror("Error al escribir en el fifo en MundoServidor");
				exit(EXIT_FAILURE);
			}
		}
	}
	for(int i=0;i<numEsferas;i++)
	{
		if(fondo_dcho.Rebota(esfera[i]))
		{
			esfera[i].centro.x=0;
			esfera[i].centro.y=rand()/(float)RAND_MAX;
			esfera[i].velocidad.x=-2-2*rand()/(float)RAND_MAX;
			esfera[i].velocidad.y=-2-2*rand()/(float)RAND_MAX;
			puntos1++;
			//Escribe el en FIFO
			m.jugador=1;
			m.puntos=puntos1;
			if(-1 == write(fd, &m, sizeof(m)))
			{
				perror("Error al escribir en el fifo en MundoServidor");
				exit(EXIT_FAILURE);
			}
		}
	}

	timer++;
	if(timer==500)
	{	
		if(numEsferas<8)
			numEsferas++;
		timer=0;
	}

	char cad2[12000];
	if(0>sprintf(cad2,"%d %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %d %d",numEsferas,esfera[0].centro.x,esfera[0].centro.y,esfera[0].radio,esfera[1].centro.x,esfera[1].centro.y,esfera[1].radio,esfera[2].centro.x,esfera[2].centro.y,esfera[2].radio,esfera[3].centro.x,esfera[3].centro.y,esfera[3].radio,esfera[4].centro.x,esfera[4].centro.y,esfera[4].radio,esfera[5].centro.x,esfera[5].centro.y,esfera[5].radio,esfera[6].centro.x,esfera[6].centro.y,esfera[6].radio,esfera[7].centro.x,esfera[7].centro.y,esfera[7].radio,jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2,puntos1,puntos2))
	{
		perror("Error en sprintf");
		exit(EXIT_FAILURE);
	}

	if(-1 == write(fd1,&cad2,12000))
	{
		perror("Error al escribir en fifo1 desde MundoServidor");
		exit(EXIT_FAILURE);
	}
}

void CMundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
//	case 's':jugador1.velocidad.y=-4;break;
//	case 'w':jugador1.velocidad.y=4;break;
//	case 'l':jugador2.velocidad.y=-4;break;
//	case 'o':jugador2.velocidad.y=4;break;
	}
}

void CMundoServidor::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;


	numEsferas=1;
	timer=0;

	//Abre el FIFO
	if ((fd=open("/tmp/FIFO", O_WRONLY))<0) {
		perror("No puede abrirse el FIFO desde MundoServidor");
		exit(EXIT_FAILURE);
	}


	const char * nombre_fifo1="/tmp/fifo1_ClienteServidor";
//	fd1=open(nombre_fifo1,O_WRONLY,0666);
	fd1=open("/tmp/fifoCS",O_WRONLY);
	if(-1 == fd1)
	{
		perror("Error al abrir el fifo1 desde MundoServidor");
		exit(EXIT_FAILURE);
	}
	
	const char * nombre_fifo2="/tmp/fifo2_ServidorCliente";
//	fd2=open(nombre_fifo2,O_RDONLY,0666);
	fd2=open("/tmp/fifoSC",O_RDONLY);
	if(-1 == fd2)
	{
		perror("Error al abrir el fifo2 desde MundoServidor");
		exit(EXIT_FAILURE);
	}

	pthread_create(&thid1,NULL,comandos,this);
}

void CMundoServidor::RecibeComandos()
{
	while(1)
	{
		usleep(10);
		char buf;
		if(-1 == read(fd2,&buf,1))
		{
			perror("Error al leer en RecibeComandos");
			exit(EXIT_FAILURE);
		}
		
		unsigned char key;
		sscanf(&buf, "%c",&key);

		switch(key)
		{
//		case 'a':jugador1.velocidad.x=-1;break;
//		case 'd':jugador1.velocidad.x=1;break;
		case 's':jugador1.velocidad.y=-4;break;
		case 'w':jugador1.velocidad.y=4;break;
		case 'l':jugador2.velocidad.y=-4;break;
		case 'o':jugador2.velocidad.y=4;break;
		}
	}
}
 
