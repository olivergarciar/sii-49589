//Oliver Garcia Rodriguez 2018
//
// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/mman.h>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
/////////////////////////////////////////////////////////////////////

int fdCom;

CMundoCliente::CMundoCliente()
{
	Init();
}

CMundoCliente::~CMundoCliente()
{	

	//Cerrar fifo1
	if(-1 == close (fd1))
	{
		perror("error al cerrar el fifo1 en MundoCliente");
		exit(EXIT_FAILURE);
	}

	if(-1 == unlink("/tmp/fifoCS"))
	{
		perror("error en el unlink de fifo1 en MundoCliente");
		exit(EXIT_FAILURE);
	}

	//Cerrar fifo2
	if(-1 == close(fd2))
	{
		perror("error al cerrar el fifo2 en MundoCiente");
		exit(EXIT_FAILURE);
	}
	
	if(-1 == unlink("/tmp/fifoSC"))
	{
		perror("error en el unlink de fifo2 en MundoCliente");
		exit(EXIT_FAILURE);
	}

}

void CMundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print1(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print1(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print1(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
//	esfera.Dibuja();
	for(int i=0;i<numEsferas;i++)
	{
		esfera[i].Dibuja();
	}

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoCliente::OnTimer(int value)
{		
	////Datos de la memoria compartida////
	p_datos->raqueta1=jugador1;
	p_datos->raqueta2=jugador2;
	//Posicion de la esfera es: centro
	float minDis1=100.0,minDis2=100.0;
	Esfera esf1,esf2;

	for(int i=0;i<numEsferas;i++) //Compara distacias entre esfera y pala
	{
		float dis=abs(esfera[i].centro.x-jugador1.x1);
		if(dis<minDis1)
		{
			minDis1=dis;
			esf1=esfera[i];
		}
	}
	p_datos->esfera1=esf1;

	for(int i=0;i<numEsferas;i++) //Lo mismo para jugador2
        {
                float dis=abs(esfera[i].centro.x-jugador2.x1);
                if(dis<minDis2)
                {
                        minDis2=dis;
                        esf2=esfera[i];
                }
        }
        p_datos->esfera2=esf2;


	////Accion del bot////
	if(p_datos->accion1==1)
		OnKeyboardDown('w',0,0);
	if(p_datos->accion1==-1)
		OnKeyboardDown('s',0,0);


	if(timer2>=100)
	{
		if(p_datos->accion2==1)
			OnKeyboardDown('o',0,0);
		if(p_datos->accion2==-1)
			OnKeyboardDown('l',0,0);
	}
	timer2++;

	char cad1[12000];
	if(-1 == read(fd1,&cad1,12000))
	{
		perror("Error al leer el fifo1 en MundoCliente");
		exit(EXIT_FAILURE);
	}
	sscanf(cad1, "%d %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %d %d",&numEsferas,&esfera[0].centro.x,&esfera[0].centro.y,&esfera[0].radio,&esfera[1].centro.x,&esfera[1].centro.y,&esfera[1].radio,&esfera[2].centro.x,&esfera[2].centro.y,&esfera[2].radio,&esfera[3].centro.x,&esfera[3].centro.y,&esfera[3].radio,&esfera[4].centro.x,&esfera[4].centro.y,&esfera[4].radio,&esfera[5].centro.x,&esfera[5].centro.y,&esfera[5].radio,&esfera[6].centro.x,&esfera[6].centro.y,&esfera[6].radio,&esfera[7].centro.x,&esfera[7].centro.y,&esfera[7].radio,&jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2,&jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2,&puntos1,&puntos2);

}

void CMundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;;break;
	case 'o':jugador2.velocidad.y=4;;break;
	}

	if(-1 == write(fd2,&key,1))
	{
		perror("Error al escribir en el fifo2 en MundoCliente");
		exit(EXIT_FAILURE);
	}
}

void CMundoCliente::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;


	numEsferas=1;
	timer=0;

	////Crear el fichero de MemCompartida////
	if((fdCom=open("/tmp/fichero", O_RDWR|O_CREAT|O_TRUNC,0666))<0){
		perror("No puede abrirse el fichero compartido desde mundoCliente");
	}

	if(-1 == write(fdCom,&datos,sizeof(DatosMemCompartida)))
	{
		perror("error al escribir en la memoria compartida en MundoCliente");
	}
	p_datos=static_cast<DatosMemCompartida*>(mmap(NULL, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fdCom, 0));
	close(fdCom);

	
	//Crear el fifo1
//	nombre_fifo1="/tmp/fifoCS";
	int mk1 = mkfifo("/tmp/fifoCS",0777);
	if(mk1 == -1)
	{
		perror("error al crear el fifo1 en MundoCliente");
		exit(EXIT_FAILURE);
	}

//	fd1=open(nombre_fifo1,O_RDONLY);
	fd1=open("/tmp/fifoCS",O_RDWR);
	if(-1 == fd1)
	{
		perror("Error a abrir el fifo1 en MundoCliente");
		exit(EXIT_FAILURE);
	}

	//Crear el fifo 2
	nombre_fifo2="/tmp/fifoSC";
//	if(-1 == mkfifo(nombre_fifo2,0666))
	int mk2 = mkfifo("/tmp/fifoSC",0777);
	if(mk2 == -1)
	{
		perror("Error al crear el fifo2 en MundoCliente");
		exit(EXIT_FAILURE);
	}

//	fd2=open(nombre_fifo2,O_WRONLY);
	fd2=open("/tmp/fifoSC",O_WRONLY);
	if(-1 == fd2)
	{
		perror("error al crear el fifo2 en MundoCliente");
		exit(EXIT_FAILURE);
	}



}
